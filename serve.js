const inquirer = require('inquirer')
const glob = require('glob')

const { spawn } = require('child_process')
const { promisify } = require('util')
const spawnAsync = promisify(spawn)

function getAllModules() {
  const files = glob.sync('src/router/*/', {
    ignore: 'src/router/ignore/'
  })

  return files.map(file => /^src\/router\/([\w-]+)\/$/.exec(file)[1])
}

(async function() {
  const { module } = await inquirer.prompt([
    {
      type: 'list',
      name: 'module',
      message: 'Select one of the modules to serve 🚀 ',
      choices: ['All']
    }
  ])

  process.env.VUE_APP_MODULE = module === 'All' ? '' : module
  await spawnAsync('./node_modules/.bin/vue-cli-service', ['serve', '--open'], {
    stdio: 'inherit'
  })
})()
