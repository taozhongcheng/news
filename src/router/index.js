import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)
import News from './news'
// 配置全部路由
const root = [
  {
    path: '*', redirect: '/news/list'
  }
]
const routes = [
  ...root,
  ...News,
]

const router = new Router({
  mode: 'history',
  routes
})

export default router
