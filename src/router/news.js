import NewsList from '@/views/news/list.vue'
import NewsDetail from '@/views/news/detail.vue'
import StatusSearch from '@/views/news/statusSearch.vue'
export default [
  {
    path: '/news/list',
    component: NewsList,
    mate: { title: '今日推荐' }
  },
  {
    path: '/news/detail',
    component: NewsDetail,
    mate: { title: '资讯详情' }
  },
  {
    path: '/news/statusSearch',
    component: StatusSearch,
    mate: { title: '文章状态查询' }
  }
]
