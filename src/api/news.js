import { Jsopn } from '@/utils/jsonp'
import {post,get} from '@/utils/request'
import { DATA_PATH, BAI_DU_PATH } from '@/config'
import Utils from '@/utils/index'
export function getNewsList(params){
  const url = `${DATA_PATH}/news/get?${Utils.jsonToUrl(params)}`
  return Jsopn(url)
} 
export function pubilish(params){
  const url = `http://47.111.184.136/baijia/publish`
  return post(url, params)
}
export function searchStatus(params) {
  const url = `/api/query/status?${Utils.jsonToUrl(params)}`
  return get(url)
} 
export function fansListall(params) {
  const url = `/api/query/articleListall`
  return get(url, params)
}
