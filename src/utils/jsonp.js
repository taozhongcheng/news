import jsonp from 'jsonp'
export function Jsopn(url) {
  return new Promise((resolve, reject) => {
    jsonp(url, null, (err, data) => {
      if (err) return reject(err)
      resolve(data)
    }
  )})
}