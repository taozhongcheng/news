import Vue from 'vue'
import axios from 'axios'
import Utils from '@/utils/index'
import { Toast } from 'vant';
import qs from 'qs'
Vue.use(Toast);

import Router from 'vue-router'
Vue.use(Router)

/* 必须参数 */
function packageParams(paramsData) {
  // 去除参数空格、undefined，空字符串
 const params = Utils.filterNull(paramsData)
 return params
}

// 设置请求头，请求数据
axios.interceptors.request.use(config => {
  //let UCOA_TICKET = '9ed51486be8240bf90dbc4d1b59a7244' //localStorage.getItem('tokenKey');
  // 2. 带上token
  // if (UCOA_TICKET) {
  //    config.headers['i-token'] = UCOA_TICKET;
  // }
 // config.data = packageParams(config.data)
  //config.headers['content-type'] = 'application/x-www-form-urlencoded'
  config.data = JSON.stringify(config.data)
  config.headers['content-type'] = 'application/json'
 // config.headers['content-type'] = 'text/plain'
  return config
}, error => {
  return Promise.reject(error);
});

axios.interceptors.response.use((response) => {
  if (response.data.errno === 0 || response.data.status === 0 || response.data.code === 0) {
    return response.data;
  }

  if (!response.config.silentError) {
    Toast(response.data.msg || response.data.errmsg || '请求失败！');
  }
  // -1为异常情况
  return -1;
}, error => {
    Toast(error.message || '请求失败！');
});

export default axios;
