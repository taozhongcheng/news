import axios from 'axios'

const axiometer = (f) => {
  return (url, params = {}, options = {}) => {
    const { ...restOptions } = options;
    const ticket = localStorage.getItem('tiken');
  //  params.ticket = ticket;
    return f(url, params, restOptions);
  };
};

export const get = axiometer((url, params, options) => {
  return axios.get(url, {
    params,
    ...options
  })
})

export const post = axiometer((url, params, options) => {
  return axios.post(url, params, { ...options })
})
