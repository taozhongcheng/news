function timeDown(leftsecond) {
  const day1 = Math.floor(leftsecond / (60 * 60 * 24))
  const hour = numToString(Math.floor((leftsecond - day1 * 24 * 60 * 60) / 3600))
  const minute = numToString(Math.floor((leftsecond - day1 * 24 * 60 * 60 - hour * 3600) / 60))
  const second = numToString(Math.floor(leftsecond - day1 * 24 * 60 * 60 - hour * 3600 - minute * 60))
  const time = `${day1}天-${hour}时-${minute}分-${second}秒`
  return time
}
function numToString(num) {
  if (num < 10) {
    num = '0' + num
  }
  return num
}
module.exports = {
  timeDown: timeDown
}
