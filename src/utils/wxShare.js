/**
 * 微信分享信息更新
 * @param params 分享内容参数
 */
// 参考文档
// 部分api即将废弃
// https://developers.weixin.qq.com/doc/offiaccount/OA_Web_Apps/JS-SDK.html
// 微信分享
export default function wxShare(params) {
  wx.ready(() => {
    const { title, desc, imgUrl, link, success = () => {} } = params

    // 自定义“分享给朋友”及“分享到QQ”按钮的分享内容 wx.onMenuShareAppMessage 即将废弃
    wx.onMenuShareAppMessage({
      title,
      desc,
      imgUrl,
      link,
      success: () => success()
    })
    // 如果废弃使用下面的方法即可
    // wx.updateAppMessageShareData({ title, desc, imgUrl, link })

    // 自定义“分享到朋友圈”按钮的分享内容 wx.onMenuShareTimeline 即将废弃
    wx.onMenuShareTimeline({
      title,
      link,
      imgUrl,
      success: () => success()
    })
    // 如果废弃使用下面的方法即可
    // wx.updateTimelineShareData({ title, desc, imgUrl, link })
  })

  wx.error(res => {
    console.log('wxShare', res)
    params.fail && params.fail(res)
  })
}

export function wxConfig(config) {
  // 注入权限验证配置
  return new Promise((resolve, reject) => {
    wx.config({
      debug: false,
      appId: config.appId, // 必填，公众号的唯一标识
      timestamp: config.timestamp, // 必填，生成签名的时间戳
      nonceStr: config.nonceStr, // 必填，生成签名的随机串
      signature: config.signature, // 必填，签名
      jsApiList: [
        'checkJsApi',
        'chooseImage',
        'uploadImage',
        'onMenuShareTimeline',
        'onMenuShareAppMessage',
        'updateAppMessageShareData',
        'updateTimelineShareData',
        'startRecord',
        'stopRecord',
        'playVoice',
        'uploadVoice',
        'hideOptionMenu',
        'showOptionMenu',
        'getNetworkType',
        'scanQRCode',
        'hideMenuItems',
        'showMenuItems',
        'openLocation'
      ]
    })
    // 微信分享
    wx.ready(function() {
      wx.checkJsApi({
        jsApiList: [
          'updateAppMessageShareData',
          'updateTimelineShareData',
          'onMenuShareTimeline',
          'onMenuShareAppMessage',
          'startRecord',
          'stopRecord',
          'playVoice',
          'uploadVoice',
          'chooseImage',
          'previewImage',
          'getNetworkType',
          'openLocation',
          'hideOptionMenu',
          'showOptionMenu',
          'hideMenuItems',
          'showMenuItems',
          'scanQRCode'
          // 'onMenuShareQQ',
          // 'onMenuShareWeibo',
          // 'onMenuShareQZone',
          // 'onVoiceRecordEnd',
          // 'pauseVoice',
          // 'stopVoice',
          // 'onVoicePlayEnd',
          // 'downloadVoice',
          // 'uploadImage',
          // 'downloadImage',
          // 'translateVoice',
          // 'getLocation',
          // 'hideAllNonBaseMenuItem',
          // 'showAllNonBaseMenuItem',
          // 'closeWindow',
          // 'chooseWXPay',
          // 'openProductSpecificView',
          // 'addCard',
          // 'chooseCard',
          // 'openCard'
        ],
        success() {},
        fail() {}
      })

      wx.hideMenuItems({
        menuList: [
          // 传播类
          // 'menuItem:share:appMessage',
          // 'menuItem:share:timeline',
          'menuItem:favorite',
          'menuItem:share:qq',
          'menuItem:share:weiboApp',
          'menuItem:share:facebook',
          'menuItem:share:QZone',

          // 保护类
          'menuItem:editTag',
          'menuItem:delete',
          'menuItem:copyUrl',
          'menuItem:originPage',
          'menuItem:readMode',
          'menuItem:openWithQQBrowser',
          'menuItem:openWithSafari',
          'menuItem:share:email',
          'menuItem:share:brand'
        ]
      })

      wx.getNetworkType({
        success: res => {
          resolve(res)
        }
      })
    })

    wx.error(res => {
      console.log('wxConfig', res)
      reject(res.errMsg)
    })
  })
}

export function updateWxShareStatus(canShare) {
  if (canShare) {
    wxShareEnable()
  } else {
    wxShareDisable()
  }
}

// 禁用分享
function wxShareDisable() {
  wx.ready(() => {
    wx.hideMenuItems({
      menuList: ['menuItem:share:appMessage', 'menuItem:share:timeline']
    })
  })
}

// 启用分享
function wxShareEnable() {
  wx.ready(() => {
    wx.showMenuItems({
      menuList: ['menuItem:share:appMessage', 'menuItem:share:timeline']
    })
  })
}
