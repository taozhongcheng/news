import COS from 'cos-js-sdk-v5'
import axios from 'axios'

// 签名
const cos = new COS({
  getAuthorization: function(options, callback) {
    axios
      .get(
        `https://download.aijiatui.com/api/Auth/signature?method=${options.Method ||
        'GET'}&pathname=${'/' + (options.Key || '')}`, {
          ignoreToken: true,
          showLoading: false
        }).then(res => {
        callback(res.signature)
      })
  }
})

/**
 * 获取实时随机字符串
 */
function createUniqueString() {
  const timestamp = +new Date() + ''
  const randomNum = parseInt((1 + Math.random()) * 65536, 10) + ''
  return (+(randomNum + timestamp)).toString(32)
}

/**
 * 获取文件上传后名称
 * @param file {file} 文件
 * @param folder {string} 文件夹名
 * @returns {string}
 */
function getFileKey(file, folder) {
  const name = createUniqueString()
  // doc取后缀不能用file.type，黑苹果doc文档会有问题,兼容裁剪只有file.type
  let fileType = ''
  if (file.name) {
    fileType = file.name.substring(file.name.lastIndexOf('.') + 1)
  } else {
    fileType = file.type.split('/')[1]
  }
  const key = `/${folder}/${name}.${fileType}`
  return key
}

/**
 * 将base64的图片转换为file文件
 * @function convertBase64UrlToBlob
 * @param  {object} urlData
 */
export function convertBase64UrlToBlob(urlData, type = 'image/jpeg') {
  const bytes = window.atob(urlData.split(',')[1]) // 去掉url的头，并转换为byte
  // 处理异常,将ascii码小于0的转换为大于0
  const ab = new ArrayBuffer(bytes.length)
  const ia = new Uint8Array(ab)
  for (var i = 0; i < bytes.length; i++) {
    ia[i] = bytes.charCodeAt(i)
  }
  return new Blob([ab], { type: type })
}

/**
 * 上传图片到腾讯云
 * @param companyId {string} 业务id用于文件夹区分可修改
 * @param cardId {string} 业务id用于文件夹区分可修改
 * @param folder {string} 文件名称
 * @param files {object} 2进制文件
 * @param fileType {string} 文件类型 image video
 * @param isMultiple {boolean} 是否多图上传,一个进度条
 * @param carryFileInfo {boolean} 是否携带文件信息返回
 * @param suffix {string} 后缀
 * @param progress {function} 上传过程中执行的方法
 * @returns {Promise<*>}
 */
export async function tencentCloud({
  folder = 'bm',
  files,
  fileType,
  isMultiple = false,
  carryFileInfo = false,
  oploadTotal,
  progress
}) {
  return new Promise((resolve, reject) => {
    // 初始化配置
    const Bucket = 'resource-1255821078' // 'resource-1255821078'
    const Region = 'ap-guangzhou' // 'ap-guangzhou'
    let datas = []
    let num = 0
    // 多文件上传
    if (isMultiple) {
      const filesArr = []
      // console.log('files', files, { ...files })
      for (let i = 0; i < files.length; i++) {
        const file = files[i]
        const key = getFileKey(file, folder)
        filesArr.push({
          Bucket: Bucket,
          Region: Region,
          Key: key,
          Body: file,
          originName: file.name
        })
      }
      // console.log('filesArr', filesArr)
      cos.uploadFiles(
        {
          files: filesArr,
          SliceSize: 1024 * 1024,
          onProgress: function(p) {
            progress &&
              progress({
                percent: +(p.percent * 100).toFixed(),
                speed: +p.speed
              })
            // console.log('进度：' + percent + '%; 速度：' + speed + 'Mb/s;')
          },
          onFileFinish: function(err, data, options) {
            // console.log(options.Key + ' 上传' + (err ? '失败' : '完成'));
            if (err !== null) {
              // eslint-disable-next-line prefer-promise-reject-errors
              reject(-1)
              return
            }
            num++
            // 这里默认按传入顺序返回
            const i = filesArr.findIndex(item => {
              return item.Key === options.Key
            })
            const file = filesArr[i]
            datas[i] = {
              url: `https://resource.aijiatui.com${options.Key}`,
              key: file.Key,
              size: file.Body.size,
              originName: options.originName,
              taskId: options.TaskId,
              folder: folder,
              file: file.Body
            }

            if (num === files.length) {
              if (!carryFileInfo) {
                datas = datas.map(item => {
                  return item.url
                })
              }
              resolve(datas)
            }
          }
        },
        function(err, data) {
          console.log(err || data)
        }
      )
    } else {
      // 分片上传
      for (let i = 0; i < files.length; i++) {
        let taskId
        const file = files[i]
        const key = getFileKey(file, folder)
        cos.sliceUploadFile(
          {
            Bucket: Bucket,
            Region: Region,
            Key: key,
            Body: file,
            onProgress: function(p) {
              // console.log(i, p)
              // 防止上传完毕多次回调
              if (p.speed) {
                progress &&
                  progress({
                    percent: +(p.percent * 100).toFixed(),
                    index: i + oploadTotal
                  })
              }
            },
            onTaskReady: function(tid) {
              taskId = tid
            }
          },
          function(err, data, options) {
            if (err !== null) {
              // eslint-disable-next-line prefer-promise-reject-errors
              reject(-1)
              return
            }
            num++
            const location =
              `https://resource.aijiatui.com${key}` +
              (fileType === 'video'
                ? `?size=${Math.round(file.size / 1024)}`
                : '')
            if (carryFileInfo) {
              datas.push({
                url: location,
                key: key,
                size: file.size,
                originName: file.name,
                taskId: taskId,
                folder: folder,
                file: file
              })
            } else {
              datas.push(location)
            }
            if (num === files.length) {
              resolve(datas)
            }
          }
        )
      }
    }
  })
}
