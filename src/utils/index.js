/*
 * @Author: coco
 * @Date: 2020-04-09 17:37:39
 * @LastEditTime: 2020-04-09 18:42:34
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
*/
export default {
  /** 路由跳转
  *  @param {object} ctx this对象
  *  @param {string} url 路由地址
  *  @param {object} ps 传递参数
  */
  toUrl(ctx, url, ps = {}) {
    const searchParams = new URLSearchParams(location.search)
    if (searchParams.get('companyId')) ps.companyId = searchParams.get('companyId')
    if (searchParams.get('cardId')) ps.cardId = searchParams.get('cardId')

    ctx.$router.push({ path: url, query: ps })
  },
  jsonToUrl(obj){
      const length = Object.getOwnPropertyNames(obj).length
      let src = ''
      Object.keys(obj).forEach((key,index) => {
          const and = length -2 > index ? '&' : ''
          src += `${key}=${obj[key]}${and}`
      })
      return src
  }
}
