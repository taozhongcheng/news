import Vue from 'vue'
import {
    Icon,
    Button,
    Search,
    Dialog,
    Toast,
    NavBar,
    Cell,
    Field,
    Checkbox,
    Form,
    CellGroup,
    List,
    DatetimePicker,
    PullRefresh
} from 'vant'

Vue.use(Icon)
Vue.use(Button)
Vue.use(Search)
Vue.use(Dialog)
Vue.use(NavBar)
Vue.use(Cell)
Vue.use(Form)
Vue.use(Field)
Vue.use(Checkbox)
Vue.use(CellGroup)

Vue.use(List)
Vue.use(DatetimePicker)
Vue.use(PullRefresh)
Vue.prototype.$toast = Toast
