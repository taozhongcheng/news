import apiUrl from '@/api/index'

export default function goToLogin(cid) {
  const redirectUrl = window.location.href
  const queryParams = {
    redirectUrl: redirectUrl,
    companyId: cid || localStorage.getItem('cid')
  }

  const qs = Object.keys(queryParams).map(k => `${k}=${encodeURIComponent(queryParams[k])}`)
  console.log('当前未登录，即将跳转到登录页面', `${apiUrl.authUrl}?${qs.join('&')}`)
  window.location.href = `${apiUrl.authUrl}?${qs.join('&')}`
}
