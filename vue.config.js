const path = require('path')
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer')
//   .BundleAnalyzerPlugin
const autoprefixer = require('autoprefixer')
const pxtorem = require('postcss-pxtorem')

module.exports = {
  runtimeCompiler: true, // 是否使用包含运行时编译器的 Vue 构建版本
  publicPath: process.env.BASE_URL,
  chainWebpack: (config) => {
    config
      .plugin('html')
      .tap((args) => {
        args[0].title = ''
        return args
      })
  },
  configureWebpack: {
    resolve: {
      alias: {
        // 别名
        '@api': path.resolve(__dirname, './src/api'),
        '@utils': path.resolve(__dirname, './src/utils'),
        '@assets': path.resolve(__dirname, './src/assets')
      }
    },
    // plugins:
    //   process.env.NODE_ENV === 'development'
    //     ? [
    //         new BundleAnalyzerPlugin()
    //         // 其他 plugins ...
    //       ]
    //     : [],
    // 将第三方库抽离出来
    externals: {
      vue: 'window.Vue',
      'vue-router': 'window.VueRouter'
      // 其他三方库 ...
    },
    devtool: 'source-map'
  },
  css: {
    loaderOptions: {
      postcss: {
        plugins: [
          autoprefixer(),
          pxtorem({
            rootValue: 37.5,
            propList: ['*', '!font*'],
            exclude: /node_modules/i
          })
        ]
      },
      scss: {
        prependData: `
        @import "~@assets/scss/variables.scss";
        @import "~@assets/scss/mixins.scss";
        @import "~@assets/scss/functions.scss";
        `
      }
    }
  },
  pluginOptions: {
    vant: {}
  },
  devServer: {
    open: true,
    host: 'localhost',
    port: 5858,
    https: false,
    //以上的ip和端口是我们本机的;下面为需要跨域的
    proxy: {//配置跨域
      '/api': {
        target: 'https://baijiahao.baidu.com/builderinner/open/resource/',//这里后台的地址模拟的;应该填写你们真实的后台接口
        ws: true,
        changOrigin: true,//允许跨域
        pathRewrite: {
          '^/api': ''//请求的时候使用这个api就可以
        },
      },
      '/data': {
        target: 'https://api.jisuapi.com',//这里后台的地址模拟的;应该填写你们真实的后台接口
        ws: true,
        changOrigin: true,//允许跨域
        pathRewrite: {
          '^/data': ''//
        },
      }
    }
  }

}
